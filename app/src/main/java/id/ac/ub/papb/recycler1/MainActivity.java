package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    public static String TAG = "RV1";
    Button bt1;
    EditText etNim;
    EditText etNama;
    ArrayList<Mahasiswa> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViewObject();
        this.data  = getData();
        renderRecyclerView(this.data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EditText nim = this.etNim;
        EditText name = this.etNama;
        ArrayList<Mahasiswa> data = this.data;
        this.bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nimValue = nim.getText().toString();
                String nameValue = name.getText().toString();
                if(nimValue.length() > 0 && nameValue.length() > 0){
                    Mahasiswa mhs = new Mahasiswa();
                    mhs.nama = nameValue;
                    mhs.nim = nimValue;
                    data.add(mhs);
                    clearTextField();
                    renderRecyclerView(data);
                }
            }
        });
    }
    private void clearTextField(){
        this.etNim.setText("");
        this.etNama.setText("");
    }
    private void renderRecyclerView(ArrayList<Mahasiswa> data){
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        this.rv1.setAdapter(adapter);
        this.rv1.setLayoutManager(new LinearLayoutManager(this));
    }
    private void bindViewObject(){
        this.rv1 = findViewById(R.id.rv1);
        this.bt1 = findViewById(R.id.bt1);
        this.etNama = findViewById(R.id.etName);
        this.etNim = findViewById(R.id.etNim);
    }
    public ArrayList<Mahasiswa> getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }
}